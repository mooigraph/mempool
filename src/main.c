/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * Or see http://www.gnu.org/licenses/gpl.txt
 *
 *   ,           ,
 *  /             \
 * ((__-^^-,-^^-__))
 * `-_---'  `---_-'
 *  `--|o`   'o|--'
 *      \  `  /
 *       ): :(
 *       :o_o:
 *        "-"
 * copyright t. lefering
 */

/*
 * easy memory subsystem, own separate file so it can be switched.
 * create pool with `new_pool()', `pmalloc()' instead of malloc
 * and `delete_pool()' to free allocated memory in pool at once.
 *
 * pool p = (pool)0;
 * void *mem1;
 * void *mem2;
 * p = new_pool();         // new pool area
 * mem1 = pmalloc (p,1234); // alloc memory
 * mem2 = pmalloc (p,2345); // alloc memory
 * p = delete_pool(p);     // delete all alloced memory
 */

#include "config.h"

#include <stdio.h>

#include "mempool.h"

int main (int argc, char *argv[])
{
 int i=0;
 pool p = (pool)0;
 void *mem1;
 void *mem2;

 p = new_pool();         // new pool area
 mem1 = pmalloc (p,1234); // alloc memory
 mem2 = pmalloc (p,2345); // alloc memory
 p = delete_pool(p);     // delete all alloced memory

 /* allocate 6 Gigabyte */
 p = new_pool();         // new pool area
 for(i=0;i<6*1024*1024;i++)
 {
  mem1 = pmalloc (p,1024); // alloc memory
 }
 p = delete_pool(p);     // delete all alloced memory
 return (0);
}

/* end. */
