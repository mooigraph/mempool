/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * Or see http://www.gnu.org/licenses/gpl.txt
 *
 *   ,           ,
 *  /             \
 * ((__-^^-,-^^-__))
 * `-_---'  `---_-'
 *  `--|o`   'o|--'
 *      \  `  /
 *       ): :(
 *       :o_o:
 *        "-"
 * copyright t. lefering
 */

/*
 * easy memory subsystem, own separate file so it can be switched.
 * create pool with `new_pool()', `pmalloc()' instead of malloc
 * and `delete_pool()' to free allocated memory in pool at once.
 *
 * pool p = (pool)0;
 * void *mem1;
 * void *mem2;
 * p = new_pool();         // new pool area
 * mem1 = pmalloc (p,1234); // alloc memory
 * mem2 = pmalloc (p,2345); // alloc memory
 * p = delete_pool(p);     // delete all alloced memory
 */

#include "config.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "mempool.h"

/* use this mempool subsystem */
#define MEMPOOLHERE 1

/* if stubs */
#ifndef MEMPOOLHERE

#warning "mempool.c is running on its stubs"

/* stub routines to switch if needed */

/* create new memory area to allocate in */
pool new_pool (void)
{
	return ((pool) 0);
}

/* allocate memory from pool */
void * pmalloc (pool p, size_t n)
{
	return ((void *) 0);
}

/* free all memory from pool */
pool delete_pool (pool p)
{
	return ((pool) 0);
}

#else /* MEMPOOLHERE */

/* forward declarations */
static pool new_pool2 (void);
static void * pmalloc2 (pool p, size_t n);
static void delete_pool2 (pool p);

/* create new memory area to allocate in */
pool new_pool (void)
{
	return ((pool) new_pool2());
}

/* allocate memory from pool */
void * pmalloc (pool p, size_t n)
{
	return ((void *) pmalloc2(p, n));
}

/* free all memory from pool */
pool delete_pool (pool p)
{
	delete_pool2 (p);
	return ((pool)0);
}

/* only local parts after this, or something else can be put here. */

/* forward declarations */
static void x_do_frees (pool p);
static void pool_register_malloc (pool p, void *ptr);

/* The flags field contains:
 * bit  31    == if set, this structure shouldn't be freed
 * bits 30-16 == number of slots in the structure
 * bits 15- 0 == number of slots used in the structure.
 */
#define _PA_NO_FREE(pa) ((pa)->flags & 0x80000000U)
#define _PA_SLOTS(pa) (((pa)->flags & 0x7fff0000U) >> 16)
#define _PA_SLOTS_USED(pa) ((pa)->flags & 0xffffU)

/* block of memory allocations. */
struct _pool_allocs
{
	struct _pool_allocs *next;
	unsigned flags;
	void *slot[0];
};

/* #define INITIAL_PA_SLOTS 16U (org) */
#define INITIAL_PA_SLOTS 64U
#define MAX_PA_SLOTS     16384U	/* Must be <= 16384 */

/* */
struct region
{
	/* memory allocations */
	struct _pool_allocs *allocs;
};

/* create completely new memory pool */
static pool new_pool2 (void)
{
	size_t size = 0;
	pool p = (pool)0;

	size = (size_t) (sizeof (struct region) + sizeof (struct _pool_allocs) + INITIAL_PA_SLOTS * sizeof (void *));

	p = malloc (size);

	/* failed malloc() */
	if (!p) { return ((pool) 0); }
	
	p->allocs = (struct _pool_allocs *) ((void *)p + sizeof (struct region));

	/*  do not dealloc this root */
	p->allocs->flags = 0x80000000U | INITIAL_PA_SLOTS << 16;

	return ((pool)p);
}

/* delete all allocations in given pool */
static void delete_pool2 (pool p)
{

	if (!p) {
		fputs ("delete_pool2() !p should not happen\n",stdout); 
		fflush (stdout);
		return;
	}

	/* addin check if p is known as a valid and active one */
	
	x_do_frees (p);

	free (p);

	return;
}

/* malloc n bytes in memory pool p */
static void * pmalloc2 (pool p, size_t n)
{
	void *ptr = (void *)0;

	if (!p) { fputs("pmalloc2() !p should not happen\n",stdout); fflush(stdout); return ((void *)0); }
	if (n==0) { fputs("pmalloc2() zero n should not happen\n",stdout); fflush(stdout); return ((void *)0); }

	/* addin check if p is known as a valid and active one */
	
	ptr = malloc (n);

	/* a failed malloc() */
	if (!ptr) { return ((void *)0); }

	pool_register_malloc (p, ptr);

	memset(ptr, 0x00, n);

	return ((void *) ptr);
}


/* */
static void x_do_frees (pool p)
{
	struct _pool_allocs *pa = (struct _pool_allocs *)0;
	struct _pool_allocs *pa_next = (struct _pool_allocs *)0;
	unsigned int i = 0;

	if (!p) {
		return;
	}

	for (pa = p->allocs; pa; pa = pa_next) /* valgrind issue here */
	{
		pa_next = pa->next;

		for (i = 0; i < _PA_SLOTS_USED (pa); ++i)
		{
			/* safety */
			if (pa->slot[i]) {
				free (pa->slot[i]);
			} else {
				fputs ("x_do_frees() nil free() should not happen\n",stdout); 
				fflush (stdout);
			}
		}

		/* keep the root which has this bit */
		if (!_PA_NO_FREE (pa)) {
			free (pa);
		}
	}

	return;
}

/* */
static void pool_register_malloc (pool p, void *ptr)
{
	int i = 0;
	unsigned int nr_slots = 0;
	struct _pool_allocs *pa = (struct _pool_allocs *)0;

	/* limit in any case (limit should normally never be reached) */
	for(i=0;i<0x00ffffff;i++)
	{
	
		/* if there is room */
		if (_PA_SLOTS_USED (p->allocs) < _PA_SLOTS (p->allocs)) {
			p->allocs->slot[_PA_SLOTS_USED(p->allocs)] = ptr;
			p->allocs->flags++;
			break;
		}

		nr_slots = _PA_SLOTS (p->allocs);

		if (nr_slots < MAX_PA_SLOTS) {
			nr_slots = nr_slots * 2; /* overflow possible? */
			/* MAX_PA_SLOTS is set to 16384 */
		}

		pa = malloc (sizeof (struct _pool_allocs) + nr_slots * sizeof (void *));

		/* malloc() failed */
		if (!pa) {
			break;
		}

		pa->next = p->allocs;
		pa->flags = nr_slots << 16;
		p->allocs = pa;
	}

	return;
}

#endif /* MEMPOOLHERE */

/* end. */
