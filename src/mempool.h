
/* memory pool subsystem */
struct region;
typedef struct region *pool;

/* fresh */
extern pool new_pool (void);

/* allocate memory from pool */
extern void * pmalloc (pool p, size_t n);

/* free all memory from pool */
extern pool delete_pool (pool p);

